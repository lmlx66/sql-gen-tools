package com.fg.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("`general_table`")
public class GeneralTable {
    @TableId
    private Long id;
    private String name;
    private Integer age;
    private String email;
    private String gender;
    private String focus;
}