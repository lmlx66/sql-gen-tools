package com.fg.mapper;


import com.fg.core.IBaseMapper;
import com.fg.entity.GeneralTable;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface GeneralTableMapper extends IBaseMapper<GeneralTable> {
}