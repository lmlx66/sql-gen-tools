package com.fg.utils;

import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.Date;

/**
 * @author 王富贵·lixiang
 * @since 2023/11/22 10:45
 * 获取当前jar运行目录工具
 */
@Slf4j
public class CurrentJarDirectoryUtil {

    public static final String dateFormat = "MM月dd日HH时mm分ss秒";

    /**
     * 大文件重推的文件夹名称
     */
    private static final String RE_PUSH_FOLDER = "DiskQueueFolder";

    /**
     * 目录分隔符，lin和win不同
     */
    public static final String directorySeparator = File.separator;

    /**
     * 行分隔符
     */
    public static final String lineSeparator = java.security.AccessController.doPrivileged(
            new sun.security.action.GetPropertyAction("line.separator"));

    /**
     * lin和win分隔符初始化
     */
    static {
        String os = System.getProperty("os.name").toLowerCase();
        log.info("当前运行环境为:{}", os);
        log.info("目录分隔符:{}", directorySeparator);
        log.info(" 行分隔符:{}", lineSeparator);
        // if (os.contains("win")) {
        //     directorySeparator = "\\";
        // } else {
        //     directorySeparator = "/";
        // }
    }

    public static String getCurrentJarDirectory() {
        ProtectionDomain protectionDomain = CurrentJarDirectoryUtil.class.getProtectionDomain();
        CodeSource codeSource = protectionDomain.getCodeSource();
        URL location = codeSource.getLocation();
        String jarPath;
        try {
            jarPath = URLDecoder.decode(location.toURI().getPath(), "UTF-8");
        } catch (Exception e) {
            // 编译后jar包处理
            /**
             * file:/C:/Users/wang/Desktop/disk-queue/disk-queue-1.0-SNAPSHOT.jar!/BOOT-INF/classes!/
             */
            jarPath = location.getPath();

            // 去除 "file:/" 前缀
            if (jarPath.startsWith("file:")) {
                jarPath = jarPath.substring(5);
            }

            // 去除 JAR 文件路径中的 "!/" 后缀
            int jarSeparatorIndex = jarPath.indexOf("!/");
            if (jarSeparatorIndex > 0) {
                jarPath = jarPath.substring(0, jarSeparatorIndex);
            }

            // 去除 JAR 文件名及其前面的路径部分
            int jarNameIndex = jarPath.lastIndexOf('/');
            if (jarNameIndex > 0) {
                jarPath = jarPath.substring(0, jarNameIndex);
            }
        }

        return jarPath;
    }

    /**
     * 指定后缀获取当前jar运行目录下的write
     */
    public static String getFileNameBySuffixXlsx(String name) {
        // 获取解压文件名
        return CurrentJarDirectoryUtil.getCurrentJarDirectory() + directorySeparator // jar运行目录
                + name + "-" +
                DateUtil.format(new Date(), dateFormat)
                + ".xlsx";
    }

    /**
     * 指定后缀获取当前jar运行目录下的write
     */
    public static String getFileNameBySuffixTxt(String name) {
        // 获取解压文件名
        return CurrentJarDirectoryUtil.getCurrentJarDirectory() + directorySeparator // jar运行目录
                + name + "-" +
                DateUtil.format(new Date(), dateFormat)
                + ".txt";
    }

    /**
     * 指定后缀获取当前jar运行目录下的write
     */
    public static String getFileNameBySuffixSQL(String name) {
        // 获取解压文件名
        return CurrentJarDirectoryUtil.getCurrentJarDirectory() + directorySeparator // jar运行目录
                + name + "-" +
                DateUtil.format(new Date(), dateFormat)
                + ".sql";
    }
}
