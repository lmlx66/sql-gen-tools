package com.fg.utils;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Console;
import cn.hutool.core.text.csv.CsvData;
import cn.hutool.core.text.csv.CsvReader;
import cn.hutool.core.text.csv.CsvRow;
import cn.hutool.core.text.csv.CsvUtil;
import cn.hutool.core.util.CharsetUtil;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Map;

/**
 * @author: 王富贵
 * @description: csv工具
 * @createTime: 2024年04月20日 21:57:01
 */
public class CSVUtils {
    public static void main(String[] args) throws FileNotFoundException {
        CsvReader reader = CsvUtil.getReader();
        // 从文件中读取CSV数据
        // CsvData data = reader.read(FileUtil.file("D:\\work_space\\sql-gen-tools\\sql-gen-tools\\src\\main\\resources\\test.csv"));
        // List<CsvRow> rows = data.getRows();
        // // 遍历行
        // for (CsvRow csvRow : rows) {
        //     // getRawList返回一个List列表，列表的每一项为CSV中的一个单元格（既逗号分隔部分）
        //     Console.log(csvRow.getRawList());
        // }

        List<Map<String, String>> maps = reader.readMapList(new FileReader(FileUtil.file("D:\\work_space\\sql-gen-tools\\sql-gen-tools\\src\\main\\resources\\test.csv")));
        maps.forEach(Console::log);
    }
}
