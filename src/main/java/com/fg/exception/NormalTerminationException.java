package com.fg.exception;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;

/**
 * @author: 王富贵
 * @description: 正常终止的异常
 * @createTime: 2024年04月20日 00:49:43
 */
public class NormalTerminationException extends MybatisPlusException {

    public NormalTerminationException() {
        super("用于打断真正执行sql的异常");
    }
}
