package com.fg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fg.entity.GeneralTable;
import com.fg.mapper.GeneralTableMapper;
import org.springframework.stereotype.Service;

/**
 * @author 王富贵·lixiang
 * @since 2024/04/29 10:06
 * 通用mapper的service类，主要用于生成batch insert
 */
@Service
public class GeneralTableService extends ServiceImpl<GeneralTableMapper, GeneralTable> implements IService<GeneralTable> {
}
