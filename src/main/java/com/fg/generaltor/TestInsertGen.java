package com.fg.generaltor;

import cn.hutool.core.text.csv.CsvUtil;
import com.fg.entity.GeneralTable;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author 王富贵·lixiang
 * @since 2024/04/29 10:10
 * 测试插入service接口
 */
@Component
public class TestInsertGen extends BaseGenerator {

    @Override
    protected void setTableName() {
        super.setTableName("test_insert_table");
    }

    @Override
    public void process() {
        setBatchCount(3);

        List<Map<String, String>> userMapsList;
        try {
            userMapsList = CsvUtil.getReader().readMapList(new FileReader(ResourceUtils.getFile("classpath:test.csv")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        ArrayList<GeneralTable> insertList = new ArrayList<>();
        for (Map<String, String> stringStringMap : userMapsList) {
            GeneralTable generalTable = new GeneralTable();
            generalTable.setId(Long.valueOf(stringStringMap.get("id")));
            generalTable.setName(stringStringMap.get("name"));
            insertList.add(generalTable);
        }
        generalTableService.saveBatch(insertList);
    }
}
