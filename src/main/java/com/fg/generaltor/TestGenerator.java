package com.fg.generaltor;

import cn.hutool.core.text.csv.CsvUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.fg.entity.GeneralTable;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

/**
 * @author: 王富贵
 * @description: 测试sql生成器
 * @createTime: 2024年04月20日 23:31:41
 */
@Component
public class TestGenerator extends BaseGenerator {
    @Override
    protected void setTableName() {
        super.setTableName("我是动态表哦");
    }

    @Override
    public void process() {
        // 测试1

        // 设置每批数量
        setBatchCount(3);
        setOutPutFileName("TestGenerator");
        setTableName("test_generator_table_name");

        List<Map<String, String>> userMapsList;
        try {
            userMapsList = CsvUtil.getReader().readMapList(new FileReader(ResourceUtils.getFile("classpath:test.csv")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        /*  目标格式
            update test_table set is_delete = 1
            where (
            (id = '1003' and name = '张三' and gender = '男' and focus = '无' and age = '33')
            or (id = '2006' and name = '李四' and gender = '男' and focus = '好对象' and age = '23')
            or (id = '3009' and name = '王妹妹' and gender = '女' and focus = '特别关注' and age = '22')
            )
         */
        batchUpdate(userMapsList,
                new BiConsumer<UpdateWrapper<GeneralTable>, List<Map<String, String>>>() {
                    @Override
                    public void accept(UpdateWrapper<GeneralTable> updateWrapper, List<Map<String, String>> maps) {
                        updateWrapper.set("is_delete", 1);
                    }
                },
                new BiConsumer<UpdateWrapper<GeneralTable>, Map<String, String>>() {
                    @Override
                    public void accept(UpdateWrapper<GeneralTable> updateWrapper, Map<String, String> stringStringMap) {
                        updateWrapper.or(updateWrapperTmp -> updateWrapperTmp.allEq(stringStringMap));
                    }
                });
        /*  目标格式
            select id,name,age,email from test_table where (id in (1003,2006,3009))
         */
        batchSelect(userMapsList,
                new BiConsumer<QueryWrapper<GeneralTable>, List<Map<String, String>>>() {
                    @Override
                    public void accept(QueryWrapper<GeneralTable> queryWrapper, List<Map<String, String>> maps) {
                        List<Long> ids = maps.stream()
                                .map(map -> map.get("id")) // 提取每个 Map 对象的 id 值
                                .filter(id -> id != null && id.matches("\\d+")) // 过滤非空且符合数字格式的 id
                                .map(Long::parseLong) // 将字符串 id 转换为 Long 类型
                                .collect(Collectors.toList()); // 收集成一个 List<Long>
                        queryWrapper.in("id", ids);
                    }
                },
                (BiConsumer<QueryWrapper<GeneralTable>, Map<String, String>>) (queryWrapper, userMaps) -> {
                }
        );


        /**
         * 测试2
         */
        // 设置每批数量
        setBatchCount(60);
        try {
            userMapsList = CsvUtil.getReader().readMapList(new FileReader(ResourceUtils.getFile("classpath:test.csv")));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        batchUpdate(userMapsList,
                new BiConsumer<UpdateWrapper<GeneralTable>, List<Map<String, String>>>() {
                    @Override
                    public void accept(UpdateWrapper<GeneralTable> updateWrapper, List<Map<String, String>> maps) {
                        updateWrapper.set("is_delete", 1);
                    }
                },
                new BiConsumer<UpdateWrapper<GeneralTable>, Map<String, String>>() {
                    @Override
                    public void accept(UpdateWrapper<GeneralTable> updateWrapper, Map<String, String> stringStringMap) {
                        updateWrapper.or(updateWrapperTmp -> updateWrapperTmp.allEq(stringStringMap));
                    }
                });

        /**
         * 测试3
         */
        // 动态表名实现表名变更
        setTableName("tools");
        QueryWrapper<GeneralTable> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", 1).eq("email_name", "li");
        generalTableMapper.delete(queryWrapper);

        /**
         * 测试4
         */
        UpdateWrapper<GeneralTable> updateWrapper = new UpdateWrapper<>();
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            ids.add(i);
        }
        updateWrapper.in("id", ids);
        updateWrapper.set("name", "富??贵");
        generalTableMapper.update(updateWrapper);
    }
}
