package com.fg.generaltor;

import com.google.common.collect.Lists;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * @author: 王富贵
 * @description: 简单的测试生成器
 * @createTime: 2024年04月21日 20:49:08
 */
@Component
public class SimpleTestGenerator extends BaseGenerator {

    @Override
    public void process() {
        setOutPutFileName("SimpleTestGenerator");
        setTableName("my_idea_table_name");
        List<Integer> list = Arrays.asList(10, 20, 30, 90, 70, 312789);
        for (List<Integer> ids : Lists.partition(list, 4)) {
            generalTableMapper.deleteBatchIds(ids);
        }
    }
}
